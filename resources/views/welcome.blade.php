<!DOCTYPE html>
<html>
@include('partials._head')
    <body class="sidebar-mini fixed">
    <div class="wrapper">
      <!-- Navbar-->
      <header class="main-header hidden-print"><a class="logo" href="/">Beetech</a>
        @include('partials._topnav')
    </header>

      <!-- Side-Nav-->
      @include('partials._sidenav')
      @include('partials._dashboardnav')
      @include('partials._contents')
         </div>
      </div>
    </div>
    <!-- Javascripts-->
    @include('partials._javascripts')
  </body>
</html>